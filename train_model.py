# -*- coding: utf-8 -*-
"""
Created on Tue Aug  1 15:03:29 2017

@author: xuanlei
"""

import lstm_model as lstm
import load_data as ld
import pandas as pd
#import random
import tensorflow as tf
from sklearn.cross_validation import train_test_split
import numpy as np
from sklearn.metrics import classification_report
from sklearn.preprocessing import normalize, MaxAbsScaler
#获得数据
window_data,f_list= ld.start()
#==============================================================================
# Global Option
#==============================================================================
feature_list = f_list
label_list = f_list


#==============================================================================
# Hyper-Para Setting 超参数设置
#==============================================================================

input_size = len(feature_list)
cell_size = 150
num_size = 4
LR = 0.001
n_steps = 30
h1_size = 35
h2_size = 17
h3_size = 9
output_size = 132
batch_size = 600

def dataframe_to_tensor(batch_data, feature_list, nm=True):
    if nm == True:
        feature_list_tran = [np.array(normalize(item[0].loc[:,feature_list]), dtype='float32') for item in batch_data]
        xs = np.array(feature_list_tran, dtype='float32')
        #xs = xs.reshape(30,30,132)
    else:
        #feature_list_tran = [(np.array(item[0].loc[feature_list], dtype='float32'))*np.ones((shape),dtype='float32') for item in batch_data]
        #temp = np.array([np.array(item[0].loc[feature_list], dtype='float32' )for item in batch_data])[1:,]
        #row = np.array([np.array(item[1].reshape(1,132), dtype='float32')for item in batch_data])
        '''
        feature_list_tran = []
        for item in batch_data:
            temp = np.array(item[0].loc[:,feature_list], dtype='float32')
            line = np.array(item[1].reshape(1,132), dtype='float32')
            combin = np.row_stack((temp[1:,],line))
            feature_list_tran.append(combin)
        xs = np.array(feature_list_tran, dtype='float32')
        xs = xs.reshape(900,132)
        '''
        feature_list_tran = [np.array(item[1].loc[:,feature_list], dtype='float32') for item in batch_data]
        xs = np.array(feature_list_tran, dtype='float32')
        xs = xs.reshape(18000,132)
    return xs
    
#==============================================================================
# start Train 训练入口
#==============================================================================
def train():
    tf.reset_default_graph()
    print('---------> 数据读取完毕，开始生成滑窗数据！')
    all_data = window_data
    model = lstm.LSTMRNN(n_steps, input_size, output_size, cell_size, h1_size, h2_size, h3_size, LR,num_size, batch_size)
    print('----------> LSTM模型架构完毕，开始训练运行模型！')
    saver = tf.train.Saver()
    with tf.Session() as sess:
        init_op = tf.global_variables_initializer()
        sess.run(init_op)
        with tf.variable_scope('scope', reuse=True):
#            saver.restore(sess, '/home/daichuantest02/wind_power/model_para/good_log')
            max_epoch = 500
            epoch = 0
            print('---------> 训练集与测试集划分完毕，开始训练模型！')
            train_sample, test_sample = train_test_split(all_data, train_size=28000, test_size=5862)
            while epoch < max_epoch:
                batch_start = 0
                for i in range(1,46):
                    train_feature_list = [item for item in train_sample[batch_start:batch_start+600]]
                    xs_input = dataframe_to_tensor(train_feature_list, feature_list)
                    ys_input = dataframe_to_tensor(train_feature_list, label_list, nm=False)
                    feed_dict_train = {model.xs:xs_input, model.ys:ys_input,
                                       model.keep_prob: np.array(0.6,  dtype='float32'), model.train_phase: True}
                    batch_start += 30  
                    _, cost, model.state = sess.run(
                            [model.train_op, model.cost, model.cells_final_state], feed_dict=feed_dict_train)
                    if i%10 == 0:
                        batch_start_test = 0
                        print('-------> now_cost: {0}'.format(cost, 2))
                        for j in range(9):
                            test_feature_list = [item for item in test_sample[batch_start_test:batch_start_test+600]]
                            xs_test_input = dataframe_to_tensor(test_feature_list, feature_list)
                            vail_predict = sess.run(model.pred, feed_dict={model.xs:xs_test_input,
                                                                           model.keep_prob:np.array(1, dtype='float32'),
                                                                           model.train_phase: False})
                            ys_test = dataframe_to_tensor(test_feature_list, label_list, nm=False)
                            '''
                            result = []
                            for item in vail_predict:
                                result.append(item)
                            '''   
                            print("------->中间结果: epoch: {0}, test_batch: {1}".format(epoch+1, j+1))
                            #print('ys:',ys_test)
                            cc = tf.sqrt(tf.reduce_mean(tf.square(tf.subtract(vail_predict,ys_test)))).eval()
                            print('result_test_cost:',cc)
                            print("#############################")
                            print(vail_predict)
                            print("#############################")
                            print(ys_test)
                            batch_start_test += 600
                #saver.save(sess, 'lstm_para/para_log')
                epoch += 1
            saver.save(sess, 'lstm_para/para_log')

        print('----------->Samples Train process has completed!<------------------') 

if __name__ == '__main__':
    train()


